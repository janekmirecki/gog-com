export enum OwnershipStatus {
  DONT_OWN = 'dont-own',
  IN_CART = 'in-cart',
  OWNS = 'owns',
}

export interface Product {
  id: string
  title: string
  price: number
  discount?: number
  status: OwnershipStatus
  coverImage: string
}

export interface EditorsPick {
  product: Product
  headerTitle: string
}
