import fetchedProducts from '@/models/products.data'
import { EditorsPick, OwnershipStatus, Product } from '@/models/shared.types'

const getPurchasedProducts = (products: Product[]): Product[] =>
  products.filter(product => {
    return product.status === OwnershipStatus.IN_CART
  })

const pickedGame: EditorsPick = {
  product: fetchedProducts[0],
  headerTitle: 'Game of the week',
}

const usersCart: Product[] = getPurchasedProducts(fetchedProducts)

export { pickedGame, usersCart }
