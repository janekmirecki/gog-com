import { OwnershipStatus, Product } from '@/models/shared.types'

const products: Product[] = [
  {
    id: 'the-witcher',
    title: 'The Witcher Adventure Game',
    price: 100,
    status: OwnershipStatus.DONT_OWN,
    coverImage: '/img/covers/the-witcher.jpg',
  },
  {
    id: 'oddworld',
    title: `Oddworld: Stranger's Wrath`,
    price: 9.99,
    status: OwnershipStatus.DONT_OWN,
    coverImage: '/img/covers/oddworld.jpg',
    discount: 0.5,
  },
  {
    id: 'chaos-on-deponia',
    title: 'Chaos on Deponia',
    price: 9.99,
    status: OwnershipStatus.OWNS,
    coverImage: '/img/covers/deponia.jpg',
  },
  {
    id: 'the-settlers2',
    title: 'The Settlers 2: Gold Edition',
    price: 5.99,
    status: OwnershipStatus.IN_CART,
    coverImage: '/img/covers/the-settlers-II.jpg',
  },
  {
    id: 'neverwinter-nights',
    title: 'Neverwinter Nights',
    price: 4.99,
    status: OwnershipStatus.DONT_OWN,
    coverImage: '/img/covers/neverwinter-nights.jpg',
    discount: 0.5,
  },
  {
    id: 'assassins-creed',
    title: `Assassin's Creed: Director's Cut`,
    price: 9.99,
    status: OwnershipStatus.IN_CART,
    coverImage: '/img/covers/assassins-creed.jpg',
  },
]

export default products
