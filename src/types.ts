import { Product } from '@/models/shared.types'

interface GogState {
  usersCart: Product[]
  products: Product[]
}

export { GogState }
