const isNumber = (value: any) => !!Number(value)

const currency = (price: string) => {
  const amount = parseFloat(price).toFixed(2)
  if (!amount || !isNumber(price)) {
    return price
  }
  return `$ ${amount}`
}

const percent = (float: number) => {
  if (!float || !isNumber(float)) {
    return float
  }
  return `${float * 100}%`
}

export { currency, percent }
