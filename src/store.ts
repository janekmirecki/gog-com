import products from '@/models/products.data'

import { usersCart } from '@/models/settings'
import { OwnershipStatus } from '@/models/shared.types'
import { GogState } from '@/types'
import Vue from 'vue'
import Vuex, { ActionContext, ActionTree, StoreOptions } from 'vuex'

Vue.use(Vuex)

export type RootState = GogState

export const rootState = (): GogState => ({ usersCart, products })

export interface Actions<S, R> extends ActionTree<S, R> {
  clearCart(context: ActionContext<S, R>): void
}

const actions: Actions<GogState, RootState> = {
  clearCart({ commit }) {
    commit('clearCart')
  },
  addToCart({ commit }, product) {
    commit('addToCart', product)
  },
  removeFromCart({ commit }, product) {
    commit('removeFromCart', product)
  },
}

const store: StoreOptions<GogState> = {
  state: rootState,
  actions,
  mutations: {
    clearCart(state) {
      state.usersCart.forEach(
        product => (product.status = OwnershipStatus.DONT_OWN)
      )
      state.usersCart = []
    },
    addToCart(state, { product }) {
      state.usersCart.push(product)
      product.status = OwnershipStatus.IN_CART
    },
    removeFromCart(state, { product }) {
      state.usersCart = state.usersCart.filter(
        cartProduct => cartProduct.id !== product.id
      )
      product.status = OwnershipStatus.DONT_OWN
    },
  },
}

export default new Vuex.Store<GogState>(store)
