import { shallowMount } from '@vue/test-utils'
import GridLayout from '@/views/GridLayout.view.vue'

describe('GridLayout.view.vue', () => {
  it('renders props.headerTitle when passed', () => {
    const headerTitle = 'new message'
    const wrapper = shallowMount(GridLayout, {
      propsData: { headerTitle },
    })
    expect(wrapper.text()).toMatch(headerTitle)
  })
})
